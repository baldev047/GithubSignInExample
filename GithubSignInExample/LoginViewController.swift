import UIKit
import WebKit
import Combine

struct ProfileData: Codable {
    
    var login = ""
    var name = ""
    var avatar_url = ""
    var email = ""
}

//MARK: - LoginView class
class LoginViewController: UIViewController {
    
    private var cancellable: AnyCancellable?
    var profilearray  = ProfileData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - Login Button
    @IBAction func githubLoginBtnAction(_ sender: UIButton) {
        githubAuthVC()
    }
    
    var webView = WKWebView()
    
    func githubAuthVC() {
        // Create github Auth ViewController
        let githubVC = UIViewController()
        // Generate random identifier for the authorization
        let uuid = UUID().uuidString
        // Create WebView
        let webView = WKWebView()
        webView.navigationDelegate = self
        githubVC.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leadingAnchor.constraint(equalTo: githubVC.view.leadingAnchor).isActive = true
        webView.trailingAnchor.constraint(equalTo: githubVC.view.trailingAnchor).isActive = true
        webView.topAnchor.constraint(equalTo: githubVC.view.topAnchor).isActive = true
        webView.bottomAnchor.constraint(equalTo: githubVC.view.bottomAnchor).isActive = true
        
        let authURLFull = "https://github.com/login/oauth/authorize?client_id=" + GithubConstants.client_ID + "&scope=" + GithubConstants.scope + "&redirect_uri=" + GithubConstants.redirect_URL + "&state=" + uuid
        
        let urlRequest = URLRequest(url: URL(string: authURLFull)!)
        webView.load(urlRequest)
        
        // Create Navigation Controller
        let navController = UINavigationController(rootViewController: githubVC)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelAction))
        githubVC.navigationItem.leftBarButtonItem = cancelButton
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.refreshAction))
        githubVC.navigationItem.rightBarButtonItem = refreshButton
        githubVC.navigationItem.title = "github.com"
        navController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        self.present(navController, animated: true, completion: nil)
    }
    //Navigation Bar ButtonAction
    @objc func cancelAction() {
        self.dismiss(animated: true, completion: nil)
    }
    @objc func refreshAction() {
        self.webView.reload()
    }
    
    //MARK: - segue for ProfileViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailseg" {
            let destVC = segue.destination as! UITabBarController
            let destView = destVC.viewControllers![0] as! ProfileViewController
            
            destView.profileModel = profilearray
        }
    }
}
//MARK: - WKNavigationDelegate
extension LoginViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        self.requestForCallbackURL(request: navigationAction.request)
        decisionHandler(.allow)
        
    }
    //MARK: - requestForCallbackURL
    func requestForCallbackURL(request: URLRequest) {
        // Get the authorization code string after the '?code=' and before '&state='
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(GithubConstants.redirect_URL) {
            if requestURLString.contains("code=") {
                if let range = requestURLString.range(of: "=") {
                    let githubCode = requestURLString[range.upperBound...]
                    if let range = githubCode.range(of: "&state=") {
                        let githubCodeFinal = githubCode[..<range.lowerBound]
                        githubRequestForAccessToken(authCode: String(githubCodeFinal))
                        
                        // Close GitHub Auth ViewController after getting Authorization Code
                        self.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
    }
    //MARK: - githubRequestForAccessToken
    func githubRequestForAccessToken(authCode: String) {
        let grantType = "authorization_code"
        
        // Set the POST parameters.
        let postParams = "grant_type=" + grantType + "&code=" + authCode + "&client_id=" + GithubConstants.client_ID + "&client_secret=" + GithubConstants.client_Secret
        let postData = postParams.data(using: String.Encoding.utf8)
        let request = NSMutableURLRequest(url: URL(string: GithubConstants.tokenURL)!)
        request.httpMethod = "POST"
        request.httpBody = postData
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, _) -> Void in
            let statusCode = (response as! HTTPURLResponse).statusCode
            if statusCode == 200 {
                let results = try! JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [AnyHashable: Any]
                let accessToken = results?["access_token"] as! String
                // Get user's id, display name, email, profile pic url
                self.fetchGitHubUserProfile(accessToken: accessToken)
            }
        }
        task.resume()
    }
    //MARK: - fetchGitHubUserProfile
    func fetchGitHubUserProfile(accessToken: String) {
        let tokenURLFull = "https://api.github.com/user?per_page=100"
        let verify: NSURL = NSURL(string: tokenURLFull)!
        let request: NSMutableURLRequest = NSMutableURLRequest(url: verify as URL)
        request.addValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
        cancellable =  URLSession.shared.dataTaskPublisher(for: request as URLRequest)
            .map { $0.data }
            .decode(type: ProfileData.self, decoder: JSONDecoder())
            .sink(receiveCompletion: { completion in
                print(completion)
            }, receiveValue: { users in
                // print(users)
                self.profilearray = users
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "detailseg", sender: self.profilearray)
                }
            })
    }
}
