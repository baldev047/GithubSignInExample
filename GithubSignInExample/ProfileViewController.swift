import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var githubDisplayNameLabel: UILabel!
    @IBOutlet weak var githubEmailLabel: UILabel!
    @IBOutlet weak var guthubAvtarImageView: UIImageView!
    @IBOutlet weak var userNameLable: UILabel!
    
    var profileModel : ProfileData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userNameLable.text = "User Login: " + profileModel!.login
        githubDisplayNameLabel.text = "Name: " + profileModel!.name
        githubEmailLabel.text = "Email: " + profileModel!.email
        let url = URL(string: profileModel!.avatar_url)
        do {
            let data = try Data.init(contentsOf: url!)
            self.guthubAvtarImageView.image = UIImage.init(data: data)
            guthubAvtarImageView.layer.cornerRadius = guthubAvtarImageView.frame.size.width / 2
            
        } catch {}
    }
}
