import UIKit

class UserDetailViewController: UIViewController {
    
    @IBOutlet var avtarImageView: UIImageView!
    @IBOutlet var followerLabel: UILabel!
    @IBOutlet var followingLabel: UILabel!
    @IBOutlet weak var activityIndicators: UIActivityIndicatorView!
    
    var userModel : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Avtar image
        let url = URL(string: userModel!.avatar_url)
        do {
            let data = try Data.init(contentsOf: url!)
            self.avtarImageView.image = UIImage.init(data: data)
            self.avtarImageView.layer.cornerRadius = avtarImageView.frame.size.width / 10
        } catch {}
        
        // Title
        self.title = userModel?.login
        
        // Followers
        getFolloersCount(url: "\(userModel!.followers_url)?per_page=100") { (array) in
            if let followers = array {
                DispatchQueue.main.async {
                    self.followerLabel.text = String(followers.count)  + " - followers"
                    self.activityIndicators.stopAnimating()
                }
            }
        }
        
        // Following
        getFollowingCount(url: userModel!.following_url) { (arrays) in
            if let following = arrays {
                DispatchQueue.main.async {
                    self.followingLabel.text = String(following.count) + " - following"
                }
            }
        }
    }
    
    //MARK: - getFolloers
    func getFolloersCount(url: String, completion:  @escaping (([[String : Any]]?) -> Void)) {
        
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                
                if let array = json as? [[String : Any]] {
                    completion(array)
                }
            } catch { completion(nil)}
        }
        task.resume()
    }
    //MARK: - getFollowung
    func getFollowingCount(url: String, completion:  @escaping (([[String : Any]]?) -> Void)) {
        
        let baseURL = "https://api.github.com/users"
        let login = userModel!.login
        let urlFollowing = "\(baseURL)/\(String(describing: login))/following?per_page=100" //?per_page=100&page=2
        
        // https://api.github.com/users/mojombo/following /other_user
        var request = URLRequest(url: URL(string: urlFollowing)!, timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                
                if let arrays = json as? [[String : Any]] {
                    completion(arrays)
                }
            } catch { completion(nil)}
        }
        task.resume()
    }
}
