import Foundation
import UIKit
import Combine

struct User: Codable {
    
    var login: String
    var avatar_url: String
    var followers_url: String
    var following_url: String
}

//MARK: - UserList class
class UsersList: UIViewController {
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var cancellable: AnyCancellable?
    var usersArray = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchUser()
        homeTableView.dataSource = self
        homeTableView.delegate = self
    }
    
    //MARK: - segue for UserDetailViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "UserDetailViewController" {
            let nextViewController = segue.destination as? UserDetailViewController
            
            if let selectedIndex = homeTableView.indexPath(for: sender as! HomeTableCell) {
                
                homeTableView.deselectRow(at: selectedIndex, animated: true)
                let user = usersArray[selectedIndex.row]
                nextViewController!.userModel = user
            }
        }
    }
    
    //MARK: - FetchUserData
    func fetchUser() {
        cancellable =  URLSession.shared.dataTaskPublisher(for: URLRequest(url: URL(string: "https://api.github.com/users?per_page=100")!))
            .map { $0.data }
            .decode(type: [User].self, decoder: JSONDecoder())
            .sink(receiveCompletion: { completion in
                print(completion)
            }, receiveValue: { users in
                // print(users)
                self.usersArray = users
                DispatchQueue.main.async {
                    self.homeTableView.reloadData()
                }
            })
    }
}

//MARK: - TableViewDelegate, DataSource Extention
extension UsersList: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell", for: indexPath) as? HomeTableCell
        
        let user = usersArray[indexPath.row]
        cell?.nameLabel.text = user.login
        
        DispatchQueue.main.async {
            cell?.profileImageView.loadImageUsingCache(withUrl: user.avatar_url)
            cell?.profileImageView.layer.cornerRadius = (cell?.profileImageView.frame.size.width)! / 3
            self.activityIndicator.stopAnimating()
        }
        /* // other download image method
         let url = URL(string: user.avatar_url)
         let task = URLSession.shared.dataTask(with: url!) { data, response, error in
         guard let data = data, error == nil else { return }
         
         DispatchQueue.main.async() {
         cell?.profileImageView.image = UIImage.init(data: data)
         cell?.profileImageView.layer.cornerRadius = ((cell?.profileImageView.frame.size.width)!) / 3
         DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
         self.activityIndicator.stopAnimating()
         }
         }
         }
         task.resume()
         */
        return cell!
    }
}

//MARK: - Download and caches image Extention for imageview cell
let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
    func loadImageUsingCache(withUrl urlString : String) {
        let url = URL(string: urlString)
        if url == nil {return}
        self.image = nil
        // check cached image
        if let cachedImage = imageCache.object(forKey: urlString as NSString)  {
            self.image = cachedImage
            return
        }
        // if not, download image from url
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            if error != nil {
                print(error!)
                return
            }
            DispatchQueue.main.async {
                if let image = UIImage(data: data!) {
                    imageCache.setObject(image, forKey: urlString as NSString)
                    self.image = image
                }
            }
        }).resume()
    }
}
//MARK: - TableViewCell
class HomeTableCell: UITableViewCell {
    
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
}
